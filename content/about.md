+++
title = 'Hobbies'
date = 2024-09-21T19:06:20-03:00
draft = false
+++
Desde cedo, sempre tive uma fascinação por carros. Não é só sobre dirigir, mas sobre entender a mecânica por trás dessas máquinas impressionantes. Gosto de acompanhar as últimas tendências automotivas, desde novos lançamentos de carros até as inovações tecnológicas que tornam a direção mais eficiente e sustentável.

Nas horas vagas, costumo assistir a programas sobre modificações automotivas, feiras de automóveis clássicos e, quando possível, participo de eventos de carros. Adoro colocar a mão na massa, fazer pequenas manutenções no meu próprio carro e, claro, dar aquela atenção especial no cuidado e na limpeza. Para mim, dirigir é uma forma de relaxar e de me reconectar com o que mais gosto.

Além dos carros, outra grande paixão é o futsal. Sempre que posso, reúno os amigos para bater uma bola. O futsal é mais do que apenas um hobby; é uma forma de manter o corpo ativo, melhorar habilidades de trabalho em equipe e, acima de tudo, me divertir.

Gosto de jogar tanto na defesa quanto no ataque, e sempre procuro melhorar minhas habilidades. Além de jogar, acompanho campeonatos e jogos de ligas profissionais, buscando inspiração em grandes jogadores.